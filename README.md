# Decommerce

An Ecommerce project that uses the best practices to creates a microservices based project using:

- Java
- Spring Boot
- Kafka
- MongoDB
- Postgresql
- Kubernetes
- Istio
- Gitlab-CI
- Axon CQRS
- Keykloak
- Gradle

Also we using of these sources of:

- Books:
  - **Practical Microservices Architectural Patterns**: Event-Based Java Microservices with Spring Boot and Spring Cloud - Apress (2019)
  - **Microservices with Spring Boot and Spring Cloud**: Build resilient and scalable microservices using Spring Cloud, Istio, and Kubernetes-Packt (2021)
  - **Full stack development with JHipster**: build full stack applications and microservices with Spring Boot and modern JavaScript frameworks (2020)
  - **Microservices Security in Action** (2020)
  - **Spring Microservices in Action**, 2nd edition (2021)
  - **Getting Started with Istio Service Mesh**: Manage Microservices in Kubernetes (2020)

- Tools:
  - JHipster

## Initiate the whole project

### 0. Git clone

Clone this repository on your local machine and then go to the `init` directory.

```sh
git clone git@gitlab.com:decommerce/init.git
cd init
```

### 1. Create a TLS certification

Create a new TLS certification for *decommerce.tk* domain in the *istio-system* namespace.

```sh
kubectl apply -f - << EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: decommmerce-crt
  namespace: istio-system
spec:
  secretName: decommerce-tls
  dnsNames:
    - decommerce.tk
    - api.decommerce.tk
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
EOF
```

### 2. Execute `init.sh`

```sh
./init.sh
```

This command:

- Create a namespace on your kubernetes called ***decommerce***.
- Create a service accouant on th *decommerce* namespace called ***gitlab***.
- Binding a cluster admin role to *gitlab* service account (limited access just to this namespace)
- Print encrypted and decrypted Gitlab token.

> Use decrypted token to create a CI/CD variable named ***GITLAB_SA_TOKEN*** in the Gitlab group of the project.

### 3. Create a client in the Keycloak

Create a Client in the [homesys realm](https://keycloak.homesys.tk/auth/admin/master/console/#/realms/homesys/clients) and name it ***decommerce***.

### 4. Set ***HOMESYS_TOR_ADDRESS***

In the Gitlab group of the project, create a CI/CD variable named ***HOMESYS_TOR_ADDRESS*** and specify its value with `https://homeos7ogafqeua3gukhi4btw3gbzoypnzsagtbmotkvtckhi5auqvid.onion:6443`.

### 5. Set ***HOMESYS_CA_K8S***

In the Gitlab group of the project, create a CI/CD variable named ***HOMESYS_CA_K8S*** and specify its value with the output of the following command:

```sh
kubectl config view --minify --raw --output 'jsonpath={..cluster.certificate-authority-data}'
```

## Obtain an OAuth access token

Use `curl` to obtain an OAuth access token from keycloak

```sh
curl \
  -s \
  -X POST \
  -H 'Content-type: application/x-www-form-urlencoded' \
  -d 'client_id=decommerce' \
  -d 'grant_type=password' \
  -d 'username=username' \
  -d 'password=passw0rd' \
  https://keycloak.homesys.tk/auth/realms/homesys/protocol/openid-connect/token \
  | jq -r '.access_token' | jq -R 'split(".") | .[0],.[1] | @base64d | fromjson'
```

## Security (Apply JWT)

Add a request authentication policy that requires end-user JWT for the ingress gateway.

### Order:

```sh
kubectl apply -f - << EOF
apiVersion: security.istio.io/v1beta1
kind: RequestAuthentication
metadata:
  name: decommerce-order
  namespace: istio-system
spec:
  selector:
    matchLabels:
      istio: ingressgateway
  jwtRules:
  - issuer: https://keycloak.homesys.tk/auth/realms/homesys
    jwksUri: https://keycloak.homesys.tk/auth/realms/homesys/protocol/openid-connect/certs
EOF

kubectl apply -f - << EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: decommerce-order
  namespace: istio-system
spec:
  selector:
    matchLabels:
      istio: ingressgateway
  action: DENY
  rules:
  - from:
    - source:
        notRequestPrincipals: ["*"]
    to:
    - operation:
        paths: ["/orders"]
EOF

# test with token
TOKEN=$(curl \
  -s \
  -X POST \
  -H 'Content-type: application/x-www-form-urlencoded' \
  -d 'client_id=decommerce' \
  -d 'grant_type=password' \
  -d 'username=username' \
  -d 'password=passw0rd' \
  https://keycloak.homesys.tk/auth/realms/homesys/protocol/openid-connect/token | jq -r '.access_token'); \
curl \
  -H "Authorization: Bearer $TOKEN" \
  https://api.decommerce.tk/orders
```


